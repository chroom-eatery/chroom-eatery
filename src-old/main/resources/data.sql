--
-- Default Roles
--
INSERT INTO role(name, description) SELECT 'ROLE_USER', 'users'
WHERE NOT EXISTS(SELECT * FROM role WHERE name = 'ROLE_USER');

INSERT INTO role(name, description) SELECT 'ROLE_ADMIN', 'admins'
WHERE NOT EXISTS(SELECT * FROM role WHERE name = 'ROLE_ADMIN');

--
-- Default Admin (password = admin)
--
INSERT INTO myuser(username, name, address, enabled, password)
SELECT 'admin', 'admin', 'database', true,
'$2b$10$.sVOuTGVGICa2zO3kVJmsOe1f5Xw5PAh2JbggIWfbGMtw8v5v44Zm'
WHERE NOT EXISTS(SELECT * FROM myuser WHERE username = 'admin');

INSERT INTO myuser_roles(user_username, roles_name)
SELECT 'admin', 'ROLE_ADMIN'
WHERE NOT EXISTS(SELECT * FROM myuser_roles WHERE user_username = 'admin');

--
-- Default Food
--
INSERT INTO food(id, name, description, price) SELECT * FROM (
    SELECT 1, 'Gurame Goreng Kremes', 'Gurame digoreng.', 15000 union
    SELECT 2, 'Lele Goreng', 'Ikan Lele digoreng.', 12000 union
    SELECT 3, 'Ayam Goreng', 'Ayam digoreng dengan sambal terasi.', 12000 union
    SELECT 4, 'Ayam Bakar', 'Ayam bakar dengan saus barbeque.', 15000 union
    SELECT 5, 'Nasi Putih', 'Nasi putih.', 5000 union
    SELECT 6, 'Nasi Goreng Jawa', 'Nasi goreng tradisional.', 25000 union
    SELECT 7, 'Mie Goreng', 'Indomie', 8000 union
    SELECT 8, 'Es Teh', 'Es teh', 4000 union
    SELECT 9, 'Teh Hangat', 'Teh Hangat', 4000 union
    SELECT 10, 'Air Bening', 'Air Putih', 3000 union
    SELECT 11, 'Pizza', 'Pizza peperoni.', 50000 union
    SELECT 12, 'Pasta', 'Pasta.', 25000 union
    SELECT 13, 'Salad', 'Salad buah.', 15000
) WHERE NOT EXISTS(SELECT * FROM food);

--
-- Default Condiment
--
INSERT INTO condiment(id, name, description, price) SELECT * FROM (
    SELECT 1, 'Sambal Terasi', 'Sambal campur terasi', 2000 union
    SELECT 2, 'Saus Sambal', 'Saus cabai merah', 2000 union
    SELECT 3, 'Saus Keju', 'Saus Keju', 5000
) WHERE NOT EXISTS(SELECT * FROM condiment);
