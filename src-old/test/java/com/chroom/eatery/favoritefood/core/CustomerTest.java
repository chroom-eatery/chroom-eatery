package com.chroom.eatery.favoritefood.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CustomerTest {
    private Customer customer;

    @BeforeEach
    public void setUp() {
        customer = new Customer("Udin");
    }

    @Test
    public void testGetName() {
        assertEquals("Udin", customer.getName());
    }

    @Test
    public void testSetName() {
        customer.setName("Udon");
        assertEquals("Udon", customer.getName());
    }

    @Test
    public void testPriceUpdate() {
        assertEquals("There has been a price change on your favorite food!",
                customer.priceUpdate());
    }

    @Test
    public void testMenuUpdate() {
        assertEquals("Check out our brand new menu!", customer.menuUpdate());
    }
}
