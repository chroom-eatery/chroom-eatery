# Choom Eatery

**Chroom Eatery** is a web based application to order food in a restaurant.\
Visit [https://chroom-eatery.herokuapp.com/](https://chroom-eatery.herokuapp.com/) (Heroku)

#### Group Members

By Group 7 of Advanced Programming Class A Fasilkom UI

- Muhammad Oktoluqman Fakhrianto
- Ryo Axtonlie
- Rhendy Rivaldo
- Donny Samuel

### Pipeline Status

| Branch                       | Pipeline                                                                                                                                                                        | Coverage                                                                                                                                                                                                   |
| ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| master (only test)           | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/master/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)                       | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/master/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/master)                                             |
| eureka-server                | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/okto/eureka/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)                  |                                                                                                                                                                                                            |
| web-service                  | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/web-service/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)                  | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/web-service/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/web-service)                                   |
| ryo/track-service            | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/ryo/track-service/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)       | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/ryo/track-service/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/ryo/track-service)             |
| okto/order-service           | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/okto/order-service/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)           | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/okto/order-service/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/okto/order-service)                     |
| donny/account-rest           | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/donny/account-rest/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines) | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/donny/account-rest/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/donny/account-rest) |
| rhendy/notifications         | [![pipeline status](https://gitlab.com/chroom-eatery/chroom-eatery/badges/rhendy/notifications/pipeline.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/pipelines)         | [![coverage report](https://gitlab.com/chroom-eatery/chroom-eatery/badges/rhendy/notifications/coverage.svg)](https://gitlab.com/chroom-eatery/chroom-eatery/commits/rhendy/notifications)                 |

### How To Run

- Clone this repository
- Open 4 terminals, and run each submodule on a different terminal:
  1. `./gradlew :eureka-server:bootRun`
  2. `./gradlew :order-service:bootRun`
  3. `./gradlew :track-service:bootRun`
  4. `./gradlew :account-service:bootRun`
  5. `./gradlew :web-service:bootRun`
- Open the server on **`http://localhost:8080/`**
