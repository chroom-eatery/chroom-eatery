package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompletedStateTest {
    private CompletedState completedState;

    @BeforeEach
    public void setUp() {
        completedState = new CompletedState(new OrderStatus((long) 1, (long) 1));
    }

    @Test
    public void testPrepare() {
        assertEquals("Order Completed", completedState.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("Order Completed", completedState.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("Order Completed", completedState.finishCooking());
    }

    @Test
    public void getStateTest() {
        assertEquals("Your order is now finished, thank you!",
                completedState.getState());
    }

    @Test
    public void cancelOrderTest() {
        assertEquals("Order Completed", completedState.cancelOrder());
    }
}
