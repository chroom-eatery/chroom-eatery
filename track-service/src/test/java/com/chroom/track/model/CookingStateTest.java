package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CookingStateTest {
    private CookingState cookingState;

    @BeforeEach
    public void setUp() {
        cookingState = new CookingState(new OrderStatus((long) 1, (long) 1));
    }

    @Test
    public void testPrepare() {
        assertEquals("Nothing to prepare", cookingState.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("Cooking order", cookingState.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("Order completed", cookingState.finishCooking());
    }

    @Test
    public void getStateTest() {
        assertEquals("Our chef is cooking your order", cookingState.getState());
    }

    @Test
    public void cancelOrderTest() {
        assertEquals("Your order is in cooking condition, cannot cancel order",
                cookingState.cancelOrder());
    }
}
