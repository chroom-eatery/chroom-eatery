package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CanceledStateTest {
    private CanceledState cancelOrder;

    @BeforeEach
    public void setUp() {
        cancelOrder = new CanceledState(new OrderStatus((long) 1, (long) 1));
    }

    @Test
    public void testPrepare() {
        assertEquals("Order canceled", cancelOrder.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("Order canceled", cancelOrder.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("Order canceled", cancelOrder.finishCooking());
    }

    @Test
    public void getStateTest() {
        assertEquals("Your order has been canceled", cancelOrder.getState());
    }

    @Test
    public void cancelOrderTest() {
        assertEquals("Order canceled", cancelOrder.cancelOrder());
    }
}
