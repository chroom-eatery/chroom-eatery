package com.chroom.track.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderStatusInStateTest {
    private OrderInState orderInState;

    @BeforeEach
    public void setUp() {
        orderInState = new OrderInState(new OrderStatus((long) 1, (long) 1));
    }

    @Test
    public void testPrepare() {
        assertEquals("Preparing", orderInState.prepare());
    }

    @Test
    public void testCooking() {
        assertEquals("You have to prepare first!", orderInState.cooking());
    }

    @Test
    public void testFinishCooking() {
        assertEquals("You have to prepare first!",
                orderInState.finishCooking());
    }

    @Test
    public void getStateTest() {
        assertEquals("We have received your order!", orderInState.getState());
    }

    @Test
    public void cancelOrderTest() {
        assertEquals("Order canceled", orderInState.cancelOrder());
    }
}
