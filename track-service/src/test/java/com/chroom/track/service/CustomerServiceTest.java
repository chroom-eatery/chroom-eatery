package com.chroom.track.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.Optional;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {
    @Mock
    private OrderStatusRepository orderStatusRepository;

    @InjectMocks
    private CustomerService customerService;

    private OrderStatus orderStatus = new OrderStatus((long) 1, (long) 1);

    @Test
    @Order(1)
    public void testAddOrder() {
        assertEquals("Order In", customerService.addOrder(1, 1));
        when(orderStatusRepository.findByCustomerId(anyLong()))
                .thenReturn(Optional.of(orderStatus));
        assertEquals("Your order isn't done yet", customerService.addOrder(2, 1));
    }

    @Test
    @Order(2)
    public void testCheckOrderStatus() throws OrderNotFoundException {
        try {
            assertEquals("You haven't ordered anything", customerService.checkOrderStatus(1));
        } catch (OrderNotFoundException err) {
            customerService.addOrder(1, 1);
            when(orderStatusRepository.findByCustomerId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));
            assertEquals("Order In", customerService.checkOrderStatus(1));
        }
    }

    @Test
    @Order(3)
    public void testCancelOrder() throws OrderNotFoundException {
        try {
            customerService.cancelOrder(1);
        } catch (OrderNotFoundException err) {
            when(orderStatusRepository.findByCustomerId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));
            customerService.cancelOrder(1);
            try {
                customerService.checkOrderStatus(1);
            } catch (OrderNotFoundException err2) {
                customerService.addOrder(2, 1);
                assertEquals("Canceled", customerService.checkOrderStatus(1));
            }
        }
    }

    @Test
    @Order(5)
    public void testGetOrderId() throws OrderNotFoundException {
        try {
            customerService.getOrderId(1);
        } catch (OrderNotFoundException err) {
            when(orderStatusRepository.findByCustomerId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));
            assertEquals(0, customerService.getOrderId(1));
        }
    }

    @Test
    @Order(6)
    public void testFinishOrder() throws OrderNotFoundException {
        try {
            customerService.finishOrder(1);
        } catch (OrderNotFoundException err) {
            when(orderStatusRepository.findByCustomerId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));
            assertEquals("Your order isn't completed yet", customerService.finishOrder(1));
        }
    }
}
