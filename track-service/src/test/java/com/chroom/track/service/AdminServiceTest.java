package com.chroom.track.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AdminServiceTest {
    @Mock
    private OrderStatusRepository orderStatusRepository;

    @InjectMocks
    private AdminService adminService;

    private OrderStatus orderStatus;
    private OrderStatus currentOrder;


    @Test
    @Order(1)
    public void testGetCurrentOrder() throws OrderNotFoundException {
        try {
            currentOrder = adminService.getCurrentOrder();
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);

            currentOrder = adminService.getCurrentOrder();
            assertEquals(orderStatus, currentOrder);
        }
    }

    @Test
    @Order(2)
    public void testSetOrder() throws OrderNotFoundException {
        try {
            currentOrder = adminService.setOrder(1);
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(orderStatus);
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);

            long orderStatusId = adminService.setOrder(1).getId();
            assertEquals(orderStatus.getId(), orderStatusId);
        }
    }

    @Test
    @Order(3)
    public void testCancelOrder() throws OrderNotFoundException {
        try {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            when(orderStatusRepository.findByAdminId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));

            adminService.setOrder(1);
            assertEquals(orderStatus.getId(), adminService.getCurrentOrderStatusId(1));

            adminService.cancelOrder(1);
            assertEquals(0, adminService.getCurrentOrderStatusId(1));

            adminService.cancelOrder(1);
        } catch (OrderNotFoundException err) {
            List<OrderStatus> orderStatusList = new ArrayList<>();
            orderStatusList.add(new OrderStatus((long) 1, (long) 1));
            when(orderStatusRepository.findAll()).thenReturn(orderStatusList);

            adminService.setOrder(1);
            assertEquals("Order In", adminService.getCurrentOrderStatusString(1));
        }
    }

    @Test
    @Order(4)
    public void testPrepareOrder() throws OrderNotFoundException {
        try {
            adminService.prepareOrder(1);
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            when(orderStatusRepository.findByAdminId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));

            adminService.setOrder(1);
            assertEquals("Order In", adminService.getCurrentOrderStatusString(1));
            adminService.prepareOrder(1);
            assertEquals("Prepare", adminService.getCurrentOrderStatusString(1));
        }
    }

    @Test
    @Order(5)
    public void testCookingOrder() throws OrderNotFoundException {
        try {
            adminService.cookingOrder(1);
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            when(orderStatusRepository.findByAdminId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));

            adminService.setOrder(1);
            adminService.prepareOrder(1);
            adminService.cookingOrder(1);
            assertEquals("Cooking", adminService.getCurrentOrderStatusString(1));
        }
    }

    @Test
    @Order(6)
    public void testFinishedCookingOrder() throws OrderNotFoundException {
        try {
            adminService.finishedCookingOrder(1);
        } catch (OrderNotFoundException err) {
            orderStatus = new OrderStatus((long) 1, (long) 1);
            when(orderStatusRepository.findByAdminId(anyLong()))
                    .thenReturn(Optional.of(orderStatus));

            adminService.setOrder(1);
            adminService.prepareOrder(1);
            adminService.cookingOrder(1);
            adminService.finishedCookingOrder(1);
        }
    }
}
