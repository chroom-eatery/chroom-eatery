package com.chroom.track.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.chroom.track.repository.OrderStatusRepository;
import com.chroom.track.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = CustomerController.class)
public class CustomerControllerTest {
    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomerService customerService;

    @Mock
    OrderStatusRepository orderStatusRepository;

    @Test
    public void customerTest() throws Exception {
        mockMvc.perform(get("/track-customer/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("customer"));
    }

    @Test
    public void addOrderTest() throws Exception {
        mockMvc.perform(get("/track-customer/1/add-order/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("addOrder"));
    }

    @Test
    public void cancelOrderTest() throws Exception {
        mockMvc.perform(get("/track-customer/1/cancel-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("cancelOrder"));
    }

    @Test
    public void finishOrderTest() throws Exception {
        mockMvc.perform(get("/track-customer/1/finish-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("finishOrder"));
    }
}
