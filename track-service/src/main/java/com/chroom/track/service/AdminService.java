package com.chroom.track.service;

import com.chroom.track.model.CanceledState;
import com.chroom.track.model.OrderState;
import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    @Autowired
    private OrderStatusRepository orderStatusRepository;

    public OrderStatus getCurrentOrder() throws OrderNotFoundException {
        try {
            List<OrderStatus> orderStatusList = orderStatusRepository.findAll();
            int index = 0;
            OrderStatus orderStatus = orderStatusList.get(index);
            while (orderStatus.getAdmin() != null
                    || !orderStatus.getState().toString().equals("Order In")) {
                index++;
                orderStatus = orderStatusList.get(index);
            }
            return orderStatus;
        } catch (IndexOutOfBoundsException err) {
            throw new OrderNotFoundException("No order yet");
        }
    }

    public OrderStatus setOrder(long adminId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        try {
            OrderStatus orderStatus = null;
            if (!orderStatusOptional.isPresent()) {
                orderStatus = getCurrentOrder();
                orderStatus.setAdmin(adminId);
                orderStatusRepository.save(orderStatus);
            }
            return orderStatus;
        } catch (OrderNotFoundException err) {
            throw new OrderNotFoundException("No order yet");
        }
    }

    public void cancelOrder(long adminId) {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (orderStatusOptional.isPresent()) {
            OrderStatus orderStatusDeleted = orderStatusOptional.get();
            orderStatusRepository.delete(orderStatusDeleted);
        }
    }

    public String prepareOrder(long adminId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (!orderStatusOptional.isPresent()) {
            throw new OrderNotFoundException("Set an order first!");
        } else {
            if (getCurrentOrderStatusString(adminId).equals("Order Canceled")) {
                return "Order Canceled";
            }
            OrderStatus orderStatus = orderStatusOptional.get();
            String message = orderStatus.prepare();
            orderStatusRepository.save(orderStatus);
            return message;
        }
    }

    public String cookingOrder(long adminId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (!orderStatusOptional.isPresent()) {
            throw new OrderNotFoundException("Set an order first!");
        } else {
            if (getCurrentOrderStatusString(adminId).equals("Order Canceled")) {
                return "Order Canceled";
            }
            OrderStatus orderStatus = orderStatusOptional.get();
            String message = orderStatus.cooking();
            orderStatusRepository.save(orderStatus);
            return message;
        }
    }

    public String finishedCookingOrder(long adminId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (!orderStatusOptional.isPresent()) {
            throw new OrderNotFoundException("Set an order first!");
        } else {
            if (getCurrentOrderStatusString(adminId).equals("Order Canceled")) {
                return "Order Canceled";
            }
            OrderStatus orderStatus = orderStatusOptional.get();
            String message = orderStatus.finishCooking();
            long orderId = orderStatus.getId();
            if (message.equalsIgnoreCase("Order completed")) {
                orderStatus.setAdmin((long) -1);
                orderStatusRepository.save(orderStatus);
            }
            return "Order with ID : " + orderId + " is finished";
        }
    }

    public String getCurrentOrderStatusString(long adminId)throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (!orderStatusOptional.isPresent()) {
            throw new OrderNotFoundException("Set an order first!");
        } else {
            OrderStatus orderStatusChecked = orderStatusOptional.get();
            OrderState orderStateChecked = orderStatusChecked.getState();
            if (orderStateChecked instanceof  CanceledState) {
                cancelOrder(adminId);
                return "Order Canceled";
            }
            return orderStateChecked.toString();
        }
    }

    public long getCurrentOrderStatusId(long adminId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOptional = orderStatusRepository.findByAdminId(adminId);

        if (!orderStatusOptional.isPresent()) {
            throw new OrderNotFoundException("Set an order first!");
        } else {
            OrderStatus orderStatusChecked = orderStatusOptional.get();
            return orderStatusChecked.getId();
        }
    }
}
