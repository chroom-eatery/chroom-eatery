package com.chroom.track.service;

import com.chroom.track.model.OrderStatus;
import com.chroom.track.repository.OrderStatusRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    private OrderStatusRepository orderStatusRepository;

    public String addOrder(long orderId, long customerId) {
        Optional<OrderStatus> orderStatusOpt = orderStatusRepository.findByCustomerId(customerId);

        if (orderStatusOpt.isPresent()) {
            return "Your order isn't done yet";
        } else {
            OrderStatus orderStatus = new OrderStatus(orderId, customerId);
            orderStatusRepository.save(orderStatus);
            return orderStatus.getState().toString();
        }
    }

    public String checkOrderStatus(long customerId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOpt = orderStatusRepository.findByCustomerId(customerId);

        if (!orderStatusOpt.isPresent()) {
            throw new OrderNotFoundException("You haven't ordered anything");
        } else {
            OrderStatus orderStatus = orderStatusOpt.get();
            return orderStatus.getState().toString();
        }
    }

    public String cancelOrder(long customerId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOpt = orderStatusRepository.findByCustomerId(customerId);

        if (!orderStatusOpt.isPresent()) {
            throw new OrderNotFoundException("You haven't ordered anything");
        } else {
            OrderStatus orderStatus = orderStatusOpt.get();
            String message = orderStatus.cancelOrder();
            if (message.equals("Order canceled")) {
                if (orderStatus.getAdmin() == null) {
                    orderStatusRepository.delete(orderStatus);
                } else {
                    orderStatus.deleteCustomer();
                    orderStatusRepository.save(orderStatus);
                }
            }
            return message;
        }
    }

    public String finishOrder(long customerId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOpt = orderStatusRepository.findByCustomerId(customerId);

        if (!orderStatusOpt.isPresent()) {
            throw new OrderNotFoundException("You haven't ordered anything");
        } else {
            OrderStatus orderStatus = orderStatusOpt.get();
            if (orderStatus.getState().toString().equals("Completed")) {
                orderStatusRepository.delete(orderStatus);
                return "Order is complete";
            } else {
                return "Your order isn't completed yet";
            }
        }
    }

    public long getOrderId(long customerId) throws OrderNotFoundException {
        Optional<OrderStatus> orderStatusOpt = orderStatusRepository.findByCustomerId(customerId);

        if (!orderStatusOpt.isPresent()) {
            throw new OrderNotFoundException("You haven't ordered anything");
        } else {
            return orderStatusOpt.get().getId();
        }
    }
}
