package com.chroom.track.repository;

import com.chroom.track.model.OrderStatus;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderStatusRepository
        extends JpaRepository<OrderStatus, Long> {

    Optional<OrderStatus> findByCustomerId(long customerId);

    Optional<OrderStatus> findByAdminId(long adminId);
}
