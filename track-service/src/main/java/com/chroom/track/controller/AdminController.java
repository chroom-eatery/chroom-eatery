package com.chroom.track.controller;

import com.chroom.track.service.AdminService;
import com.chroom.track.service.OrderNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/track-admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @GetMapping("{adminId}")
    public Map<String, String> admin(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString(adminId));
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId(adminId)));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    "Handle an order please");
        }
        return map;
    }

    @GetMapping("/{adminId}/set-order")
    public Map<String, String> setOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            adminService.setOrder(adminId);
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString(adminId));
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId(adminId)));
            map.put("message",
                    "Order has been set");
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    "No order yet");
        }
        return map;
    }

    @GetMapping("/{adminId}/prepare-order")
    public Map<String, String> prepareOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message",
                    adminService.prepareOrder(adminId));
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString(adminId));
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId(adminId)));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }

    @GetMapping("/{adminId}/cooking-order")
    public Map<String, String> cookingOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message",
                    adminService.cookingOrder(adminId));
            map.put("current_order_status_state",
                    adminService.getCurrentOrderStatusString(adminId));
            map.put("current_order_status_id",
                    String.valueOf(adminService.getCurrentOrderStatusId(adminId)));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }

    @GetMapping("/{adminId}/finished-cooking-order")
    public Map<String, String> finishedCookingOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
            map.put("message",
                    adminService.finishedCookingOrder(adminId));
        } catch (OrderNotFoundException err) {
            map.put("current_order_status_state",
                    "-");
            map.put("current_order_status_id",
                    "-");
        }
        return map;
    }
}
