package com.chroom.track.controller;

import com.chroom.track.service.CustomerService;
import com.chroom.track.service.OrderNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/track-customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/{customerId}")
    public Map<String, String> customer(
            @PathVariable("customerId") long customerId
    ) {
        Map<String, String> map = new HashMap<>();
        map.put("message", "");
        map.put("customer_id", String.valueOf(customerId));
        try {
            map.put("order_id",
                    String.valueOf(customerService.getOrderId(customerId)));
            map.put("order_status",
                    customerService.checkOrderStatus(customerId));
        } catch (OrderNotFoundException err) {
            map.put("order_id",
                    "-");
            map.put("order_status",
                    "-");
            map.put("message",
                    "You haven't ordered anything");
        }
        return map;
    }

    @GetMapping("{customerId}/add-order/{orderId}")
    public Map<String, String> addOrder(
            @PathVariable("customerId") long customerId,
            @PathVariable("orderId") long orderId
    ) throws OrderNotFoundException {
        Map<String, String> map = new HashMap<>();
        map.put("message", customerService.addOrder(orderId, customerId));
        map.put("order_id", String.valueOf(customerService.getOrderId(customerId)));
        map.put("order_status", customerService.checkOrderStatus(customerId));
        return map;
    }

    @GetMapping("{customerId}/cancel-order")
    public Map<String, String> cancelOrder(
            @PathVariable("customerId") long customerId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message", customerService.cancelOrder(customerId));
        } catch (OrderNotFoundException err) {
            map.put("message", "You haven't ordered anything");
        }
        return map;
    }

    @GetMapping("{customerId}/finish-order")
    public Map<String, String> finishOrder(
            @PathVariable("customerId") long customerId
    ) {
        Map<String, String> map = new HashMap<>();
        try {
            map.put("message", customerService.finishOrder(customerId));
        } catch (OrderNotFoundException err) {
            map.put("message", "You haven't ordered anything");
        }
        return map;
    }
}
