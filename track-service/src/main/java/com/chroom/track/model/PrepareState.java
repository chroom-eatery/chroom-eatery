package com.chroom.track.model;

public class PrepareState implements OrderState {
    private OrderStatus orderStatus;
    private String cookFirst = "You have to cook first!";

    public PrepareState(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String prepare() {
        return "Preparing";
    }

    public String cooking() {
        this.orderStatus.setState("cooking");
        return "Cooking order";
    }

    public String finishCooking() {
        return cookFirst;
    }

    public String getState() {
        return "Currently preparing your order!";
    }

    public String cancelOrder() {
        this.orderStatus.setState("canceled");
        return "Order canceled";
    }

    public String toString() {
        return "Prepare";
    }
}
