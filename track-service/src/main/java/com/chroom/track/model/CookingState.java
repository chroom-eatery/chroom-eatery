package com.chroom.track.model;

public class CookingState implements OrderState {
    private OrderStatus orderStatus;

    public CookingState(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String prepare() {
        return "Nothing to prepare";
    }

    public String cooking() {
        return "Cooking order";
    }

    public String finishCooking() {
        this.orderStatus.setState("completed");
        return "Order completed";
    }

    public String getState() {
        return "Our chef is cooking your order";
    }

    public String cancelOrder() {
        return "Your order is in cooking condition, cannot cancel order";
    }

    public String toString() {
        return "Cooking";
    }
}
