package com.chroom.track.model;

public interface OrderState {

    String prepare();

    String cooking();

    String finishCooking();

    String getState();

    String cancelOrder();

}
