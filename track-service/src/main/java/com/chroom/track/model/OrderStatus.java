package com.chroom.track.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_status")
public class OrderStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private Long orderId;

    @Column
    private String currentState;

    @Column
    private Long adminId;

    @Column
    private Long customerId;

    public OrderStatus(){}

    public OrderStatus(Long orderId, Long customerId) {
        this.orderId = orderId;
        this.customerId = customerId;
        currentState = "order in";
    }

    public void setAdmin(Long admin) {
        this.adminId = admin;
    }

    public Long getAdmin() {
        return this.adminId;
    }

    public void deleteCustomer() {
        this.customerId = null;
    }

    public OrderState getState() {
        switch (this.currentState) {
            case "prepare":
                return new PrepareState(this);
            case "cooking":
                return new CookingState(this);
            case "completed":
                return new CompletedState(this);
            case "canceled":
                return new CanceledState(this);
            default :
                return new OrderInState(this);
        }
    }

    public void setState(String currentState) {
        this.currentState = currentState;
    }

    public String prepare() {
        return getState().prepare();
    }

    public String cooking() {
        return getState().cooking();
    }

    public String finishCooking() {
        return getState().finishCooking();
    }

    public String cancelOrder() {
        return getState().cancelOrder();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
