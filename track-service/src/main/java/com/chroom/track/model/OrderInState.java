package com.chroom.track.model;

public class OrderInState implements OrderState {
    private OrderStatus orderStatus;
    private String prepareText = "You have to prepare first!";

    public OrderInState(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String prepare() {
        this.orderStatus.setState("prepare");
        return "Preparing";
    }

    public String cooking() {
        return prepareText;
    }

    public String finishCooking() {
        return prepareText;
    }

    public String getState() {
        return "We have received your order!";
    }

    public String cancelOrder() {
        this.orderStatus.setState("canceled");
        return "Order canceled";
    }

    public String toString() {
        return "Order In";
    }
}
