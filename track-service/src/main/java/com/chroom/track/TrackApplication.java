package com.chroom.track;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class TrackApplication {
    public static void main(String[] args) {
        SpringApplication.run(TrackApplication.class, args);
    }
}
