package com.chroom.eatery.account.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.chroom.eatery.account.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    public void login() throws Exception {
        mockMvc.perform(get("/account/login"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("login"));
    }

    @Test
    public void logout() throws Exception {
        mockMvc.perform(get("/account/logout"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("logout"));
    }

    @Test
    public void registerCustomerGet() throws Exception {
        mockMvc.perform(get("/account/register"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("registerCustomer"));
    }

    @Test
    public void registerCustomerPost() throws Exception {
        this.mockMvc.perform(post("/account/register").param("username", "johndoe")
                .param("password", "passwordku")
                .param("name","John Doe")
                .param("address","Universitas Indonesia, Depok"))
                .andExpect(status().isOk());
    }

    @Test
    public void registerAdminGet() throws Exception {
        mockMvc.perform(get("/account/register/admin"))
                .andExpect(redirectedUrl("/account/login"))
                .andExpect(handler().methodName("registerAdmin"));
    }

    @Test
    public void registerAdminPost() throws Exception {
        this.mockMvc.perform(post("/register/admin").param("username", "admin_john")
                .param("password", "passwordku")
                .param("name","Admin John Doe")
                .param("address","Universitas Indonesia, Depok")).andExpect(redirectedUrl("redirect:/account/"));
    }

}
