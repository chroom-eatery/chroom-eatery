package com.chroom.web.controller.track;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

@WebMvcTest(value = CustomerController.class)
public class TrackCustomerControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void currentOrderTest() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("customer_id", "1");
        map.put("order_id", "1");
        map.put("order_status", "Order In");

        when(restTemplate.getForObject(
                "http://track-service/track-customer/1", Map.class))
                .thenReturn(map);

        mockMvc.perform(get("/track-customer/1"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("customer"));
    }

    @Test
    public void addOrderTest() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("customer_id", "1");
        map.put("order_id", "1");
        map.put("order_status", "Order In");

        when(restTemplate.getForObject(
                "http://track-service/track-customer/1/add-order/1", Map.class))
                .thenReturn(map);

        mockMvc.perform(get("/track-customer/1/add-order/1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("addOrder"));
    }

    @Test
    public void cancelOrderTest() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("customer_id", "1");
        map.put("order_id", "1");
        map.put("order_status", "Order In");

        when(restTemplate.getForObject(
                "http://track-service/track-customer/1/cancel-order", Map.class))
                .thenReturn(map);

        mockMvc.perform(get("/track-customer/1/cancel-order"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("cancelOrder"));
    }

    @Test
    public void finishOrderTest() throws Exception {
        Map<String, String> map = new HashMap<>();
        map.put("customer_id", "1");
        map.put("order_id", "1");
        map.put("order_status", "Order In");

        when(restTemplate.getForObject(
                "http://track-service/track-customer/1/finish-order", Map.class))
                .thenReturn(map);

        mockMvc.perform(get("/track-customer/1/finish-order"))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("finishOrder"));
    }
}
