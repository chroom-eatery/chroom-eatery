package com.chroom.web.controller.track;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

@WebMvcTest(value = AdminControllerRest.class)
public class TrackAdminControllerRestTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void currentOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/1/current-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("currentOrder"));
    }

    @Test
    public void setOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/1/set-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("setOrder"));
    }

    @Test
    public void prepareOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/1/prepare-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("prepareOrder"));
    }

    @Test
    public void cookingOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/1/cooking-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("cookingOrder"));
    }

    @Test
    public void finishedCookingOrderTest() throws Exception {
        mockMvc.perform(get("/track-admin/1/finished-cooking-order"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("finishedCookingOrder"));
    }
}