package com.chroom.web.controller.order;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

@WebMvcTest(value = OrderControllerRest.class)
public class OrderControllerRestTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void menu() throws Exception {
        mockMvc.perform(get("/order-api/")
                .contentType("application/json"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("menu"));
    }

    @Test
    public void addFood() throws Exception {
        mockMvc.perform(get("/order-api/add/1/1"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("addFood"));
    }

    @Test
    public void deleteFood() throws Exception {
        mockMvc.perform(get("/order-api/delete/1/1"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("deleteFood"));
    }

    @Test
    public void order() throws Exception {
        mockMvc.perform(get("/order-api/order/1"))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("getOrder"));
    }

}
