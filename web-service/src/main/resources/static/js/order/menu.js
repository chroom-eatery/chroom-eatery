$(document).ready(() => {
  if (user_id === null) {
    window.location.href = "/login";
  }

  $.when(getMenu(), getOrder()).done(function (menu, order) {
    let foods = menu[0];
    let orders = order[0].foodOrderSet;

    $("#menu").empty();
    console.log(orders);

    for (let food of foods) {
      let row = $('<div class="row" />');
      row.append('<div class="col-1">' + food.id + "</div>");
      row.append('<div class="col-3">' + food.name + "</div>");
      row.append('<div class="col-5">' + food.description + "</div>");
      row.append('<div class="col-1">' + food.price + "</div>");

      let foodFound = false;
      for (let foodOrder of orders) {
        if (foodOrder.food.id === food.id) {
          row.append('<div class="col-1">' + foodOrder.amount + "</div>");
          foodFound = true;
          break;
        }
      }
      if (!foodFound) {
        row.append('<div class="col-1">0</div>');
      }
      row.append(
        '<div class="col-1 text-primary action" onclick="add(' +
          food.id +
          ')">add</div>'
      );
      $("#menu").append(row);
    }
    console.log(order);
  });
});

function getMenu() {
  return $.ajax({
    method: "GET",
    url: "/order-api/",
    dataType: "json",
  });
}

function getOrder() {
  return $.ajax({
    method: "GET",
    url: "/order-api/order/" + user_id,
    dataType: "json",
  });
}

function add(id) {
  $.ajax({
    method: "GET",
    url: "/order-api/add/" + id + "/" + user_id,
    dataType: "json",
    success: function (response) {
      console.log(response);
      $("#menu > .row").each(function () {
        if ($(this).children().eq(0).text() === "" + id) {
          $(this).children().eq(4).text(response.amount);
        }
      });
    },
    error: function (a, b) {
      console.log(a, b);
    },
  });
}
