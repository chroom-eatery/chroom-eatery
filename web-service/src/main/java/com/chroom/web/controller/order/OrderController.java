package com.chroom.web.controller.order;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class OrderController {

    @GetMapping("/menu")
    public String menu() {
        return "order/menu";
    }

    @GetMapping("/order")
    public String getOrder() {
        return "order/order";
    }

}
