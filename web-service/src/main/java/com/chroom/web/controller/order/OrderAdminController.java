package com.chroom.web.controller.order;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping("/admin")
public class OrderAdminController {

    private final String serviceUrl = "http://order-service";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String admin(Model model) {
        model.addAttribute("adminId", 1);
        return "order/admin";
    }

    @GetMapping("/add-food")
    public String addFood() {
        return "order/add-food.html";
    }

    @PostMapping(path = "/add-food")
    public String addFood(
            @RequestParam(value = "name") String name,
            @RequestParam(value = "description") String description,
            @RequestParam(value = "price") int price) throws Exception {

        Map<String, Object> foodMap = new HashMap<>();
        foodMap.put("name", name);
        foodMap.put("description", description);
        foodMap.put("price", price);
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(foodMap);

        restTemplate.postForEntity(
            serviceUrl + "/admin/add-food",
            request,
            Map.class);

        return "redirect:/admin";
    }

}
