package com.chroom.web.controller.account;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "/account")
public class AccountControllerRest {

    private final String serviceUrl = "http://account-service";

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping("/login")
    public Map<?, ?> login(@RequestParam(value = "username") String username,
                             @RequestParam(value = "password") String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> payload = new LinkedMultiValueMap<>();

        payload.add("username", username);
        payload.add("password", password);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(payload, headers);
        Map<?, ?> loginData = restTemplate.postForObject(
                serviceUrl + "/account/login",
                request,
                Map.class);

        System.out.println(loginData);
        return loginData;
    }

    @PostMapping("/register")
    public Map<?, ?> registerUser(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> payload = new LinkedMultiValueMap<>();

        payload.add("username", username);
        payload.add("password", password);
        payload.add("name", name);
        payload.add("address", address);

        System.out.println(payload);


        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(payload, headers);
        Map<?, ?> registerData = restTemplate.postForObject(
                serviceUrl + "/account/register",
                request,
                Map.class);

        System.out.println(registerData);

        return registerData;

    }

    @PostMapping("/register/admin")
    public Map<?, ?> registerAdmin(@RequestParam(value = "username") String username,
                                   @RequestParam(value = "password") String password,
                                   @RequestParam(value = "name") String name,
                                   @RequestParam(value = "address") String address) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> payload = new LinkedMultiValueMap<>();

        payload.add("username", username);
        payload.add("password", password);
        payload.add("name", name);
        payload.add("address", address);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(payload, headers);
        Map<?, ?> registerData = restTemplate.postForObject(
                serviceUrl + "/account/register/admin",
                request,
                Map.class);

        System.out.println(registerData);
        return registerData;
    }
}
