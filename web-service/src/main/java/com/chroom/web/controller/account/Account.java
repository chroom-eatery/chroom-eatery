package com.chroom.web.controller.account;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class Account {

    @GetMapping
    public String loginRegister() {
        return "account/account";
    }
}