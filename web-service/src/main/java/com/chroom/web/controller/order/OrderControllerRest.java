package com.chroom.web.controller.order;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("order-api/")
public class OrderControllerRest {

    private final String serviceUrl = "http://order-service";

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public List<?> menu(Model model) {
        return restTemplate.getForObject(serviceUrl, List.class);
    }

    @GetMapping("/add/{foodId}/{userId}")
    public Map<?, ?> addFood(@PathVariable long foodId, @PathVariable long userId) {
        HttpEntity<String> request = new HttpEntity<>("");
        Map<?, ?> foodOrder = restTemplate.postForObject(
                serviceUrl + "/" + userId + "/add/" + foodId,
                request,
                Map.class);

        System.out.println(foodOrder);
        return foodOrder;
    }

    @GetMapping("/delete/{foodId}/{userId}")
    public Map<?, ?> deleteFood(@PathVariable long foodId, @PathVariable long userId) {
        HttpEntity<String> request = new HttpEntity<>("");
        Map<?, ?> foodOrder = restTemplate.postForObject(
                serviceUrl + "/" + userId + "/delete/" + foodId,
                request,
                Map.class);

        System.out.println(foodOrder);
        return foodOrder;
    }

    @GetMapping("/order/{userId}")
    public Map<?, ?> getOrder(@PathVariable long userId) {
        Map<?, ?> order =
                restTemplate.getForObject(serviceUrl + "/" + userId, Map.class);

        System.out.println(order);
        return order;
    }

}
