package com.chroom.web.controller.track;

import com.netflix.discovery.converters.Auto;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("track-customer")
public class CustomerController {
    @Auto
    RestTemplate restTemplate;

    public CustomerController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/{customerId}")
    public String customer(
            @PathVariable("customerId") long customerId,
            Model model) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/"
                + customerId, Map.class);

        String orderStatus = map.get("order_status");

        model.addAttribute("customer_id", map.get("customer_id"));
        model.addAttribute("order_id", map.get("order_id"));
        model.addAttribute("order_status", orderStatus);

        return "track/customer";
    }

    @GetMapping("{customerId}/add-order/{orderId}")
    public String addOrder(
            @PathVariable("customerId") long customerId,
            @PathVariable("orderId") long orderId,
            RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/"
                + customerId + "/add-order/" + orderId, Map.class);

        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer/" + customerId;
    }

    @GetMapping("{customerId}/cancel-order")
    public String cancelOrder(
            @PathVariable("customerId") long customerId,
            RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/"
                + customerId + "/cancel-order", Map.class);

        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer/" + customerId;
    }

    @GetMapping("{customerId}/finish-order")
    public String finishOrder(
            @PathVariable("customerId") long customerId,
            RedirectAttributes redirectAttributes) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-customer/"
                + customerId + "/finish-order", Map.class);
        redirectAttributes.addFlashAttribute("message", map.get("message"));

        return "redirect:/track-customer/" + customerId;
    }
}
