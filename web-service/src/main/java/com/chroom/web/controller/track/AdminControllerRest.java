package com.chroom.web.controller.track;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(path = "/track-admin")
public class AdminControllerRest {
    @Autowired
    RestTemplate restTemplate;

    public AdminControllerRest(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/{adminId}/current-order")
    public Map<String, String> currentOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/"
                + adminId, Map.class);
        return map;
    }

    @GetMapping("/{adminId}/set-order")
    public Map<String, String> setOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/"
                + adminId + "/set-order", Map.class);
        return map;
    }

    @GetMapping("/{adminId}/prepare-order")
    public Map<String, String> prepareOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/"
                + adminId + "/prepare-order", Map.class);

        return map;
    }

    @GetMapping("/{adminId}/cooking-order")
    public Map<String, String> cookingOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/"
                + adminId + "/cooking-order", Map.class);

        return map;
    }

    @GetMapping("/{adminId}/finished-cooking-order")
    public Map<String, String> finishedCookingOrder(
            @PathVariable("adminId") long adminId
    ) {
        Map<String, String> map = restTemplate.getForObject("http://track-service/track-admin/"
                + adminId + "/finished-cooking-order", Map.class);

        return map;
    }
}
