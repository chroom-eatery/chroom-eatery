package com.chroom.order.controller;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.chroom.order.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = OrderController.class)
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrderService orderSevice;

    @Test
    public void menu() throws Exception {
        mockMvc.perform(get("/")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("menu"));

        verify(orderSevice).findAllFood();
    }

    @Test
    public void addFood() throws Exception {
        mockMvc.perform(post("/1/add/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("addFood"));

        verify(orderSevice).addFoodFromUser(1, 3);
    }

    @Test
    public void deleteFood() throws Exception {
        mockMvc.perform(post("/1/delete/3")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("deleteFood"));

        verify(orderSevice).deleteFoodFromUser(1, 3);

    }

    @Test
    public void getCurrentOrder() throws Exception {
        mockMvc.perform(get("/1")
                .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(handler().methodName("getOrder"));

        verify(orderSevice).getCurrentOrder(1);
    }
}
