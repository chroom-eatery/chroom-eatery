package com.chroom.order.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FoodOrderTest {

    private FoodOrder foodOrder;
    private Food testFood;

    @BeforeEach
    public void setUp() {
        testFood = new Food("Nasi Goreng", "Nasi digoreng", 12000);
        Condiment condiment = new Condiment("Keju", "Parutan keju.", 2000);
        List<Condiment> condiments = new ArrayList<>();
        condiments.add(condiment);

        foodOrder = new FoodOrder();
        foodOrder.setAmount(1);
        foodOrder.setFood(testFood);
        foodOrder.setCondiments(condiments);
    }

    @Test
    public void testContructorWithParameters() {
        Food food = new Food("Bakso", "Daging sapi direbus.", 15000);
        Condiment condiment = new Condiment("Sosis", "Sosis sapi.", 3000);
        List<Condiment> condiments = new ArrayList<>();
        condiments.add(condiment);

        foodOrder = new FoodOrder(food, condiments, 1);

        assertEquals(food, foodOrder.getFood());
        assertEquals(condiments, foodOrder.getCondiments());
        assertEquals(1, foodOrder.getAmount());
    }

    @Test
    public void testGetId() {
        assertEquals(0, foodOrder.getId());
    }

    @Test
    public void testSetId() {
        foodOrder.setId(2);
        assertEquals(2, foodOrder.getId());
    }

    @Test
    public void testGetFood() {
        assertEquals(testFood, foodOrder.getFood());
    }

    @Test
    public void testSetFood() {
        Food newFood = new Food("Bakso", "Daging sapi direbus.", 15000);
        foodOrder.setFood(newFood);
        assertEquals(newFood, foodOrder.getFood());
    }

    @Test
    public void testGetAmount() {
        assertEquals(1, foodOrder.getAmount());
    }

    @Test
    public void testSetAmount() {
        foodOrder.setAmount(2);
        assertEquals(2, foodOrder.getAmount());
    }

    @Test
    public void testSetCondiments() {
        Condiment condiment = new Condiment("Sosis", "Sosis sapi.", 3000);
        List<Condiment> condiments = new ArrayList<>();
        condiments.add(condiment);
        foodOrder.setCondiments(condiments);
        assertEquals(15000, foodOrder.getPrice());
    }

    @Test
    public void testGetCondiments() {
        List<Condiment> condiments = foodOrder.getCondiments();
        assertEquals("Keju", condiments.get(0).getName());
    }
}
