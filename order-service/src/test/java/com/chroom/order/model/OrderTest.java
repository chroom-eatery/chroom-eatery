package com.chroom.order.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderTest {

    private Order order;
    private Set<FoodOrder> foodList;

    @BeforeEach
    public void setUp() {
        order = new Order();
        order.setIdUser(123L);

        Food testFood = new Food("Nasi Goreng", "Nasi digoreng", 12000);

        FoodOrder testFoodOrder = new FoodOrder();
        testFoodOrder.setAmount(1);
        testFoodOrder.setFood(testFood);
        testFoodOrder.setCondiments(new ArrayList<Condiment>());

        foodList = new HashSet<>();
        foodList.add(testFoodOrder);
        order.setFoods(foodList);
    }

    @Test
    public void testGetId() {
        assertEquals(0, order.getId());
    }

    @Test
    public void testSetId() {
        order.setId(2);
        assertEquals(2, order.getId());
    }

    @Test
    public void testGetIdUser() {
        assertEquals(123L, order.getIdUser());
    }

    @Test
    public void testSetIdUser() {
        order.setIdUser(456L);
        assertEquals(456L, order.getIdUser());
    }

    @Test
    public void testGetFoodOrders() {
        assertEquals(foodList, order.getFoodOrderSet());
    }

    @Test
    public void testGetTotalPrice() {
        assertEquals(12000, order.getTotalPrice());
    }
}
