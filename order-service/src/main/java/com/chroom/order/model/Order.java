package com.chroom.order.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "myorder")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private long idUser;

    @OneToMany(orphanRemoval = true)
    private Set<FoodOrder> foodOrderSet;

    public Order() {
        this.foodOrderSet = new HashSet<>();
    }

    public Order(long userId) {
        this();
        this.idUser = userId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public Set<FoodOrder> getFoodOrderSet() {
        return foodOrderSet;
    }

    public void setFoods(Set<FoodOrder> foodOrderSet) {
        this.foodOrderSet = foodOrderSet;
    }

    public int getTotalPrice() {
        int totalPrice = 0;
        for (FoodOrder foodOrder : this.foodOrderSet) {
            totalPrice += foodOrder.getPrice() * foodOrder.getAmount();
        }
        return totalPrice;
    }

    public void addFoodOrder(FoodOrder foodOrder) {
        foodOrderSet.add(foodOrder);
    }

}
