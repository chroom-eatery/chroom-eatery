package com.chroom.order.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "food_order")
public class FoodOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    private Food food;

    @Column
    private int amount;

    @OneToMany(orphanRemoval = true)
    private List<Condiment> condiments;

    public FoodOrder() {
    }

    public FoodOrder(Food food, List<Condiment> condiments, int amount) {
        this.food = food;
        this.condiments = condiments;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public List<Condiment> getCondiments() {
        return this.condiments;
    }

    public void setCondiments(List<Condiment> condiments) {
        this.condiments = condiments;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPrice() {
        int totalPrice = this.food.getPrice();
        for (Condiment condiment : this.condiments) {
            totalPrice += condiment.getPrice();
        }
        return totalPrice;
    }

}
