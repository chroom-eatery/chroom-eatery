package com.chroom.account.core;

public class UserCreationException extends Exception {

    public UserCreationException() {
        super();
    }

    public UserCreationException(String message) {
        super(message);
    }

}
