package com.chroom.account.core;

import com.chroom.account.model.AppUser;
import com.chroom.account.model.Role;
import com.chroom.account.repository.RoleRepository;
import com.chroom.account.repository.UserRepository;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserFactory {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    public AppUser createUser(String type, String username, String password,
                              String name, String address) throws UserCreationException {

        if (userRepository.findByUsername(username).isPresent()) {
            throw new UserCreationException("Username already exist.");
        }

        Set<Role> roles = new HashSet<>();

        switch (type) {
            case "admin":
                if (!roleRepository.findByName("ROLE_ADMIN").isPresent()) {
                    roleRepository.save(new Role("ROLE_ADMIN", "ADMIN"));
                }
                roles.add(roleRepository.findByName("ROLE_ADMIN").get());
                break;
            case "user":
                if (!roleRepository.findByName("ROLE_USER").isPresent()) {
                    roleRepository.save(new Role("ROLE_USER", "USER"));
                }
                roles.add(roleRepository.findByName("ROLE_USER").get());
                break;
            default:
                throw new UserCreationException("User type '" + type
                        + "' not exist.");
        }

        AppUser appUser = new AppUser(username, password, name, roles, address);
        userRepository.save(appUser);

        return appUser;
    }

}
