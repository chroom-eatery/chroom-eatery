package com.chroom.account.controller;


import com.chroom.account.core.UserCreationException;
import com.chroom.account.model.AppUser;
import com.chroom.account.model.JwtResponse;
import com.chroom.account.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path = "/account")
public class AccountController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String home(Model model) {
        Authentication auth =
            SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("username", auth.getName());
        return "account/home";
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password
    ) throws Exception {
        final String jwt = userService.authenticateToken(username, password);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @GetMapping("/register")
    public String registerCustomer() {
        return "account/register";
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerCustomer(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) {
        try {
            final String jwt = userService.register("user", username, password, name, address);
            return ResponseEntity.ok(new JwtResponse(jwt));
        } catch (UserCreationException e) {
            return ResponseEntity.status(400).body("Username has been already taken!");
        }
    }

    @GetMapping("/register/admin")
    public String registerAdmin(Model model) {
        return "account/register-admin";
    }

    @PostMapping("/register/admin")
    public ResponseEntity<?> registerAdmin(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "name") String name,
            @RequestParam(value = "address") String address) throws Exception {
        try {
            final String jwt = userService.register("admin", username, password, name, address);
            return ResponseEntity.ok(new JwtResponse(jwt));
        } catch (UserCreationException e) {
            return ResponseEntity.status(400).body("Username has been already taken!");
        }
    }

}
