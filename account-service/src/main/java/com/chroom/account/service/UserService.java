package com.chroom.account.service;

import com.chroom.account.core.UserCreationException;
import com.chroom.account.core.UserFactory;
import com.chroom.account.model.AppUser;
import com.chroom.account.security.JwtUtil;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    @Autowired
    private AuthenticationManager authManager;

    @Autowired
    private UserFactory userFactory;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private JwtUtil jwtUtil;

    public String authenticateToken(String username, String password) throws Exception {
        try {
            authManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password)
            );
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username or password");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String jwt = jwtUtil.generateToken(userDetails);
        return jwt;
    }

    public String register(String type, String username, String password,
                            String name, String address) throws UserCreationException {
        String encodedPassword = passwordEncoder.encode(password);
        userFactory.createUser(type, username, encodedPassword,
                    name, address);

        authManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        final String jwt = jwtUtil.generateToken(userDetails);

        return jwt;
    }

}
